/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexos.software.system.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Erwin De León
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LogErroresDto {
    
    
    private String fila;
    private String columna;
    private String error;
    
}
