/*
* Archivo: Validacion.java
* Fecha: 07/11/2020
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva de nexos software
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de nexos software
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
 */
package com.nexos.software.system.model;

 
 
import java.util.ArrayList; 
import java.util.List; 
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.codec.digest.DigestUtils; 

public class Validacion {

    private boolean valido = true;
    private final List<String> errores = new ArrayList<String>(0);

    public Validacion(boolean valido) {
        this.valido = valido;
    }

    public boolean isValido() {
        return valido;
    }

    public void setValido(boolean valido) {
        this.valido = valido;
    }

    public List<String> getErrores() {
        return errores;
    }

    public void addError(String error) {
        valido = false;
        errores.add(error);
    }

    public static boolean isNull(String v) {
        if (v == null || v.equals("") || v == "") {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isMail(String mail) {
        String regex = "^(.+)@(.+)$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(mail);
        if (matcher.matches()) {
            return true;
        } else {
            return false;
        }
    }

    public static String codificarRegistroId(String clave) {
        // Se utiliza el identificador del usuario como salt          
        return DigestUtils.sha256Hex(clave);
    }

    public static String getDomain(String correo) {
        String temp = correo.substring(correo.indexOf("@") + 1); // e.g. @google.com
        String dominio = temp.substring(0, temp.indexOf("."));  // e.g. google
        return dominio;
    }

}
