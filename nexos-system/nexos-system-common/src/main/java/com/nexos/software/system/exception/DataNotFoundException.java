/*
* Archivo: DataNotFoundException.java
* Fecha: 07/11/2020
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva de nexos software
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de nexos software
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
*/
package com.nexos.software.system.exception;

public class DataNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final Class<?> exceptionClass;
	private final Class<?> notFoundClass;
	
	public DataNotFoundException(Class<?> exceptionClass, Class<?> notFoundClass) {
		super();
		this.exceptionClass = exceptionClass;
		this.notFoundClass = notFoundClass;
	}

	public Class<?> getExceptionClass() {
		return exceptionClass;
	}
	
	public Class<?> getNotFoundClass() {
		return notFoundClass;
    }

}
