/*
* Archivo: SecurityUtil.java
* Fecha: 07/11/2020
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva de NEXOS SOFTWARE
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de NEXOS SOFTWARE
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
 */
package com.nexos.software.system.util;

import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

/**
 * Funciones para encripcion y desencripcion.
 *
 * @author Erwin De León
 */
public abstract class SecurityUtil {

    private static final String ALGORTIHM = "AES/CBC/PKCS5Padding";

    public static String decrypt(String key, String initVector, String encrypted) throws Exception {

        IvParameterSpec iv = new IvParameterSpec(initVector.getBytes());
        SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
        Cipher cipher = Cipher.getInstance(ALGORTIHM);
        cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

        byte[] original = cipher.doFinal(Base64.decodeBase64(encrypted));

        return new String(original);

    }

    /**
     * Metodo para desencriptar el token enviado desde el front, en este metodo se usa solo el key
     * el vector lo genera automatico
     * @param encrypted
     * @param key
     * @return String
     * @throws java.security.GeneralSecurityException
     */
    public static String decrypt2(String key, byte[] encrypted)
            throws GeneralSecurityException {

        byte[] raw = key.getBytes(Charset.forName("UTF-8"));
        if (raw.length != 16) {
            throw new IllegalArgumentException("Invalid key size.");
        }
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");

        Cipher cipher = Cipher.getInstance(ALGORTIHM);
        cipher.init(Cipher.DECRYPT_MODE, skeySpec, new IvParameterSpec(new byte[16]));
        byte[] original = cipher.doFinal(encrypted);

        return new String(original, Charset.forName("UTF-8"));
    }

    public static String encrypt(String key, String initVector, String value) throws Exception {
        try {
            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes());
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
            Cipher cipher = Cipher.getInstance(ALGORTIHM);
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
            byte[] encrypted = cipher.doFinal(value.getBytes());
            return Base64.encodeBase64String(encrypted);

        } catch (NoSuchAlgorithmException | NoSuchPaddingException ex) {
            throw new Exception("Error decifrando el texto", ex);
        }

    }

    /**
     * Metodo para encriptar el token enviado desde el front, en este metodo se usa solo el key
     * el vector lo genera automatico
     * @param key
     * @param value     
     * @return byte[]
     * @throws java.lang.Exception
     * @throws java.security.GeneralSecurityException
     */
    public static byte[] encrypt2(String key, String value) throws Exception, GeneralSecurityException {
        try {
            byte[] raw = key.getBytes(Charset.forName("UTF-8"));
            if (raw.length != 16) {
                throw new IllegalArgumentException("Invalid key size.");
            }

            SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES"); 
            Cipher cipher = Cipher.getInstance(ALGORTIHM);
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, new IvParameterSpec(new byte[16]));
            return cipher.doFinal(value.getBytes(Charset.forName("UTF-8")));
            //return Base64.encodeBase64String(encrypted);

        } catch (NoSuchAlgorithmException | NoSuchPaddingException ex) {
            throw new Exception("Error decifrando el texto", ex);
        }

    }
}
