/*
* Archivo: Contexto.java
* Fecha: 07/11/2020
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva de nexos software
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de nexos software
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
 */
package com.nexos.software.system.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Representa el contexto. Puede contener el usuario, persona logueado, la empresa, etc.
 *
 * @author Erwin De León
 */
public class Contexto {

    private final Map<String, String> contenido = new HashMap<>();

    public static final String USUARIO_ID = "usuarioId";
    public static final String CLIENTE_IP = "clienteIp";
    public static final String EMPRESA_ID = "empresaId";
    public static final String ROL_ID = "rolId";

    public void put(String llave, String valor) {
        contenido.put(llave, valor);
    }

    public String get(String llave) {
        return contenido.get(llave);
    }
}
