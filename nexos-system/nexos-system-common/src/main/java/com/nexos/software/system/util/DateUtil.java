/*
* Archivo: DateUtil.java
* Fecha: 07/11/2020
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva de NEXOS SOFTWARE
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de NEXOS SOFTWARE
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
*/
package com.nexos.software.system.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Funcionaes utilitarias para manejo de fechas.
 * 
 * @author Erwin De León
 */
public abstract class DateUtil {

    private static final TimeZone TIMEZONE = TimeZone.getTimeZone("America/Bogota");
    private static final ZoneId ZONE_ID = ZoneId.of("America/Bogota");

    /**
     * Constructor privado. Patrón singleton.
     */
    private DateUtil() {

    }

    /**
     * Obtiene la fecha actual del sistema.
     * 
     * @return Fecha actual.
     */
    public static Date now() {
        return Calendar.getInstance(TIMEZONE).getTime();
    }

    /**
     * Obtiene la fecha actual del sistema.
     * 
     * @return Fecha actual.
     */
    public static LocalDateTime nowDateTime() {
        return now().toInstant().atZone(ZONE_ID).toLocalDateTime();
    }

    /**
     * Convierte una fecha de tipo Date a LocalDate
     * 
     * @param date Fecha de tipo Date
     * @return Fecha de tipo LocalDate
     */
    public static LocalDateTime dateToLocalDateTime(Date date) {
        return date.toInstant().atZone(ZONE_ID).toLocalDateTime();
    }

    /**
     * Convierte una fecha de tipo Date a LocalDate
     * 
     * @param date Fecha de tipo Date
     * @return Fecha de tipo LocalDate
     */
    public static LocalDate dateToLocalDate(Date date) {
        return date.toInstant().atZone(ZONE_ID).toLocalDate();
    }

    /**
     * Convierte una fecha de tipo Date a LocalTime.
     * 
     * @param date Fecha de tipo Date
     * @return Fecha de tipo LocalTime
     */
    public static LocalTime dateToLocalTime(Date date) {
        return date.toInstant().atZone(ZONE_ID).toLocalTime();
    }

    /**
     * Convierte una cadena con formato AAAA-MM-DD a LocalDate.
     * 
     * @param fecha Cadena con la fecha.
     * @return LocalDate
     */
    public static LocalDate stringToLocalDate(String fecha) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return LocalDate.parse(fecha, formatter);
    }
}
