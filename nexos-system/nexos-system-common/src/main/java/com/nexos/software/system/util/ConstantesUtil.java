/*
* Archivo: ConstantesUtil.java
* Fecha: 07/11/2020
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva de NEXOS SOFTWARE
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de NEXOS SOFTWARE
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
 */
package com.nexos.software.system.util;
 

public abstract class ConstantesUtil {

    public static final String ESTADO_ACTIVO = "activo";
    public static final String ESTADO_INACTIVO = "inactivo";
    /**
     * Dirección del SMTP para envio de correo.
     */
    public static final String MAIL_SMTP_HOST = "mail.smtp.host";
    /**
     * Puerto del SMTP para envio de correo.
     */
    public static final String MAIL_SMTP_PORT = "mail.smtp.port";
    /**
     * Indica si el SMTP requiere de autenticación.
     */
    public static final String MAIL_SMTP_AUTH = "mail.smtp.auth";
    /**
     * Permite uso de TSL, esta variable se utiliza ya que algunos antivirus
     * pueden bloquear el envio de correo sin esta propiedad.
     */
    public static final String MAIL_SMTP_SSL_TRUST = "mail.smtp.ssl.trust";
    /**
     * Indica si el SMTP requiere uso de TSL.
     */
    public static final String MAIL_SMTP_ENABLE = "mail.smtp.starttls.enable";
    /**
     * Usuario para envio de correo.
     */
    public static final String MAIL_SMTP_USER = "mail.smtp.user";
    /**
     * Clave del usuario para envio de correo.
     */
    public static final String MAIL_SMTP_PASSWORD = "mail.smtp.password";

    public static final String MAIL_SMTP_TIMEOUT = "mail.smtp.timeout";

    public static final String MAIL_SMTP_CONNECTIONTIMEOUT = "mail.smtp.connectiontimeout";

    public static final String MAIL_SMTPS_TIMEOUT = "mail.smtps.timeout";

    public static final String MAIL_SMTPS_CONNECTIONTIMEOUT = "mail.smtps.connectiontimeout";

    public static final Short AUD_CATEGORIA_IDENTIDAD = 1;
    public static final Short AUD_IDENTIDAD_SUBCATEGORIA_ACCESO_VALIDO = 1;
    public static final Short AUD_IDENTIDAD_SUBCATEGORIA_ACCESO_INVALIDO = 2;
    public static final Short AUD_IDENTIDAD_SUBCATEGORIA_ACCESO_BLOQUEADO = 3;
    public static final Short AUD_IDENTIDAD_SUBCATEGORIA_ACCESO_INACTIVO = 4;
    public static final Short AUD_IDENTIDAD_SUBCATEGORIA_ACCESO_LOGOUT = 5;

    public static final Short AUD_CATEGORIA_SEGURIDAD = 2;
    public static final Short AUD_SUBCATEGORIA_SEGURIDAD_TOKEN_CADUCADO = 1;
    public static final Short AUD_SUBCATEGORIA_SEGURIDAD_TOKEN_INVALIDO = 2;
    public static final Short AUD_SUBCATEGORIA_SEGURIDAD_INICIO_SESION_EXITOSO = 3;
    public static final Short AUD_SUBCATEGORIA_SEGURIDAD_INICIO_SESION_ERROR_INACTIVO = 4;
    public static final Short AUD_SUBCATEGORIA_SEGURIDAD_INICIO_SESION_ERROR_ROL = 5;
    public static final Short AUD_SUBCATEGORIA_SEGURIDAD_CREDENCIALES_ERRONEAS = 6;
    public static final Short AUD_SUBCATEGORIA_SEGURIDAD_CIERRE_SESION = 7;

    /**
     * Numero maximo de intentos de autenticacion invalidos seguidos para
     * bloquear la cuenta.
     */
    public static final String SYS_IDENTIDAD_INTENTOS_BLOQUEO = "sys.identidad.intentosBloqueo";
    /**
     * Tiempo de espera de una cuenta bloqueada para que se pueda autenticar de
     * nuevo el usuario.
     */
    public static final String SYS_IDENTIDAD_ESPERA_BLOQUEO = "sys.identidad.esperaBloqueo";
    /**
     * Indica si el usuario solo se puede loguear una vez de manera concurrente
     */
    public static final String SYS_IDENTIDAD_UNICO_LOGUEO = "sys.identidad.unicoLogueo";
    /**
     * Tiempo en segundos que caduca la session si no hay actividad del usuario.
     * Si es 0 no caduca
     */
    public static final String SYS_IDENTIDAD_TIEMPO_SESSION = "sys.identidad.tiempoSession";
    /**
     * Tiempo en segundos para consultar si hay nuevas notificaciones para
     * mostrar al colaborador dentro del portal
     */
    public static final String URL_PORTAL_TIEMPO_NOTIFICACION = "url.portal.tiempoNotificacion";
    /**
     * Indica si se debe almacenar auditoria de los CRUD en las entidades. Debe
     * retornar un boolean.
     */
    public static final String SYS_AUDITORIA_ENTIDADES = "sys.auditoria.entidades";

    /**
     *
     */
    public static final String SYS_JWT_TOKEN_SECRET_KEY = "sys.jwt.token.secretKey";
    /**
     * Lenguaje por defecto.
     */
    public static final String SYS_LANGUAGE = "sys.language";
    /**
     * Timezon por defecto.
     */
    public static final String SYS_TIMEZONE = "sys.timezone";

}
