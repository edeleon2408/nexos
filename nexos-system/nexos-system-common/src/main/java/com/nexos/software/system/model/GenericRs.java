/*
* Archivo: GenericRs.java
* Fecha: 07/11/2020
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva de nexos software
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de nexos software
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
*/
package com.nexos.software.system.model;

import java.io.Serializable;

/**
 * Respuesta generica.
 * 
 * @author Erwin De León
 */
public class GenericRs implements Serializable {

    private static final long serialVersionUID = 6894049659423270002L;

    public int codigo;
    public String cuerpo;

    public GenericRs() {
        super();
    }

    public GenericRs(int codigo, String cuerpo) {
        super();
        this.codigo = codigo;
        this.cuerpo = cuerpo;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getCuerpo() {
        return cuerpo;
    }

    public void setCuerpo(String cuerpo) {
        this.cuerpo = cuerpo;
    }

}
