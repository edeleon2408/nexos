package com.nexos.software.system.service;
 
import com.nexos.software.system.entity.StandardEntity;
import com.nexos.software.system.model.Contexto;


/**
 * Servicio para persistencia de entidades de negocio y registro de auditoria 
 * automatico.
 * 
 * @author Erwin De Leon
 */
public interface PersistenciaUtilityService {

    /**
     * Actualizar una entidad de negocio.
     * @param <T> StandardEntity.
     * @param contexto Contexto de la peticion.
     * @param entity Entidad de negocio a persistir.
     * @return Entidad persistente.
     */
    <T extends StandardEntity> T actualizar(Contexto contexto, T entity);
    /**
     * Crear una nueva entidad de negocio.
     * @param <T> StandardEntity.
     * @param contexto Contexto de la peticion.
     * @param entity Entidad de negocio a persistir.
     * @return Entidad persistente.
     */
    <T extends StandardEntity> T crear(Contexto contexto, T entity);
    /**
     * Elimina una entidad de negocio.
     * @param <T> StandardEntity.
     * @param contexto Contexto de la peticion.
     * @param entity Entidad de negocio a persistir.
     */
    <T extends StandardEntity> void eliminar(Contexto contexto, T entity);
    
}
