/*
* Archivo: LoginDatosRs.java
* Fecha: 13/05/2019
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva de NEXOS SOFTWARE
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de NEXOS SOFTWARE
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
*/
package com.nexos.software.system.message;

import java.time.LocalDateTime;

import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModelProperty;

/**
 * Se duplica logica de colaborador para iniciar sesion en admin web, 
 * hago esto para no alterar portal colaborador
 * 
 * @author Erwin De León
 */
public class LoginDatosRs {
	@NotBlank
	@ApiModelProperty(notes = "codigo", required = true)
	private int codigo;

	@NotBlank
	@ApiModelProperty(notes = "valor", required = true)
	private String valor;

	@NotBlank
	@ApiModelProperty(notes = "usuarioToken", required = true)
	private String usuarioToken;

	@NotBlank
	@ApiModelProperty(notes = "primerNombre", required = true)
	private String primerNombre;

	@NotBlank
	@ApiModelProperty(notes = "primerApellido", required = true)
	private String primerApellido;

	@NotBlank
	@ApiModelProperty(notes = "correo", required = true)
	private String correo;

	@NotBlank
	@ApiModelProperty(notes = "ultimoIngreso", required = true)
	private LocalDateTime ultimoIngreso;

	@NotBlank
	@ApiModelProperty(notes = "tiempoSession", required = true)
	private String tiempoSession;

	@NotBlank
	@ApiModelProperty(notes = "tiempoNotificacion", required = true)
	private String tiempoNotificacion;

	@NotBlank
	@ApiModelProperty(notes = "rol", required = true)
	private String rolId;
	
	@NotBlank
	@ApiModelProperty(notes = "rol", required = true)
	private String rolNombre;
	
	@NotBlank
	@ApiModelProperty(notes = "rol", required = true)
	private String tipoRol;

	public LoginDatosRs(int codigo, String valor, String usuarioToken, String primerNombre, String primerApellido,
			String correo, LocalDateTime ultimoIngreso, String tiempoSession, String tiempoNotificacion, String rolId, String rolNombre, String tipoRol) {
		super();
		this.codigo = codigo;
		this.valor = valor;
		this.usuarioToken = usuarioToken;
		this.primerNombre = primerNombre;
		this.primerApellido = primerApellido;
		this.correo = correo;
		this.ultimoIngreso = ultimoIngreso;
		this.tiempoSession = tiempoSession;
		this.tiempoNotificacion = tiempoNotificacion;
		this.rolId = rolId;
		this.rolNombre = rolNombre;
		this.tipoRol = tipoRol;
	}

	public String getUsuarioToken() {
		return usuarioToken;
	}

	public void setUsuarioToken(String usuarioToken) {
		this.usuarioToken = usuarioToken;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getPrimerNombre() {
		return primerNombre;
	}

	public void setPrimerNombre(String primerNombre) {
		this.primerNombre = primerNombre;
	}

	public String getPrimerApellido() {
		return primerApellido;
	}

	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public LocalDateTime getUltimoIngreso() {
		return ultimoIngreso;
	}

	public void setUltimoIngreso(LocalDateTime ultimoIngreso) {
		this.ultimoIngreso = ultimoIngreso;
	}

	public String getTiempoSession() {
		return tiempoSession;
	}

	public void setTiempoSession(String tiempoSession) {
		this.tiempoSession = tiempoSession;
	}

	public String getTiempoNotificacion() {
		return tiempoNotificacion;
	}

	public void setTiempoNotificacion(String tiempoNotificacion) {
		this.tiempoNotificacion = tiempoNotificacion;
	}

	public String getRolId() {
		return rolId;
	}

	public void setRolId(String rolId) {
		this.rolId = rolId;
	}

	public String getRolNombre() {
		return rolNombre;
	}

	public void setRolNombre(String rolNombre) {
		this.rolNombre = rolNombre;
	}

	public String getTipoRol() {
		return tipoRol;
	}

	public void setTipoRol(String tipoRol) {
		this.tipoRol = tipoRol;
	}
	

}
