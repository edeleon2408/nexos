/*
* Archivo: PersonaNucleFamiliarRs.java
* Fecha: 08/11/2020
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva de NEXOS SOFTWARE
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de GRUPO NEXOS SOFTWARE
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
*/
package com.nexos.software.system.message;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotBlank;

/**
 *
 * @author Erwin De León
 */
public class PersonaNucleoFamiliarRs {
    
    private static final long serialVersionUID = 4922443619920845700L;
    
    @NotBlank
    @ApiModelProperty(notes = "Identificación de la Persona", required = true)
    private String identificacion;
    
    @NotBlank
    @ApiModelProperty(notes = "Primer nombre de la Persona", required = true)
    private String primerNombre;
    
    @ApiModelProperty(notes = "Segundo nombre de la Persona", required = false)
    private String segundoNombre;
    
    @NotBlank
    @ApiModelProperty(notes = "Primer apellido de la Persona", required = true)
    private String primerApellido;
        
    @ApiModelProperty(notes = "Segundo apellido de la Persona", required = false)
    private String segundoApellido;
    
    @ApiModelProperty(notes = "Dirección de la Persona", required = false)
    private String direccion;
    
    @ApiModelProperty(notes = "Telefono de la Persona", required = false)
    private int telefono;
    
    @ApiModelProperty(notes = "Email de la Persona", required = false)
    private String email;
    
    @ApiModelProperty(notes = "Fecha nacimiento de la Persona", required = false)
    private String fechaNacimiento;
    
    @ApiModelProperty(notes = "Estado(activo o inactivo) de la Persona", required = false)
    private String estado;
    
    @ApiModelProperty(notes = "Tipo Relación entre las dos Personas", required = false)
    private String tipoRelacion;

    public PersonaNucleoFamiliarRs(String identificacion, String primerNombre, String segundoNombre, String primerApellido, String segundoApellido, String direccion, int telefono, String email, String fechaNacimiento, String estado, String tipoRelacion) {
        this.identificacion = identificacion;
        this.primerNombre = primerNombre;
        this.segundoNombre = segundoNombre;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.direccion = direccion;
        this.telefono = telefono;
        this.email = email;
        this.fechaNacimiento = fechaNacimiento;
        this.estado = estado;
        this.tipoRelacion = tipoRelacion;
    }

    
    
    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getTipoRelacion() {
        return tipoRelacion;
    }

    public void setTipoRelacion(String tipoRelacion) {
        this.tipoRelacion = tipoRelacion;
    }
    
    
    
}
