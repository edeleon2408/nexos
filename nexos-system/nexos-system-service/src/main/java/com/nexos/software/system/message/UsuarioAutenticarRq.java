/*
* Archivo: UsuarioAutenticarRq.java
* Fecha: 08/11/2020
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva de NEXOS SOFTWARE
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de NEXOS SOFTWARE
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
*/
package com.nexos.software.system.message;

import java.io.Serializable;

/**
 * Mensaje de peticion de autenticacion.
 * 
 * @author Erwin De León
 */
public class UsuarioAutenticarRq implements Serializable {
    
    private static final long serialVersionUID = -1L;
    
    /**
     * Token de autenticacion. Este token trae el usuario y la clave unidos por dos puntos,
     * encriptados utilizando algoritmo blowfish, es decir una funcion al estilo: blowfish(usuario:clave).
     */
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }    
    
}
