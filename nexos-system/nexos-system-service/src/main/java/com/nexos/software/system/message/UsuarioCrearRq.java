/*
* Archivo: UsuarioCrearRq.java
* Fecha: 08/11/2020
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva de NEXOS SOFTWARE
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de NEXOS SOFTWARE
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
 */
package com.nexos.software.system.message;

import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import io.swagger.annotations.ApiModelProperty;

/**
 * Mensaje de peticion.
 *
 * @author Erwin De León
 *
 */
public class UsuarioCrearRq implements Serializable {

    private static final long serialVersionUID = 4922443619920845700L;
    @NotBlank
    @ApiModelProperty(notes = "Nombre de usuario", required = true)
    private String usuario;
    private String email;
    @NotBlank
    @ApiModelProperty(notes = "Clave del usuario", required = true)
    private String clave;
    private String movil;
    private String tipoDocumento;
    private String documento;
    private String primerNombre;
    private String primerApellido;
    private String segundoNombre;
    private String segundoApellido;

    public UsuarioCrearRq() {
        super();
    }

    public UsuarioCrearRq(String usuario, String email, String clave, String tipoDocumento,
            String documento, String movil) {
        super();
        this.usuario = usuario;
        this.email = email;
        this.clave = clave;
        this.tipoDocumento = tipoDocumento;
        this.documento = documento;
        this.movil = movil;
    }

    public UsuarioCrearRq(String usuario, String email, String clave, String movil) {
        super();
        this.usuario = usuario;
        this.email = email;
        this.clave = clave;
        this.movil = movil;
    }

    public UsuarioCrearRq(String usuario, String email, String clave) {
        super();
        this.usuario = usuario;
        this.email = email;
        this.clave = clave;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getMovil() {
        return movil;
    }

    public void setMovil(String movil) {
        this.movil = movil;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

}
