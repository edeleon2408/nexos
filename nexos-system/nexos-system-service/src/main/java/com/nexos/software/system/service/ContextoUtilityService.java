/*
* Archivo: ContextoUtilityService.java
* Fecha: 07/11/2020
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva de NEXOS SOFTWARE
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de NEXOS SOFTWARE
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
 */
package com.nexos.software.system.service;
 
import com.nexos.software.system.model.Contexto;
import javax.servlet.ServletRequest;

/**
 * Servicio utilitario para manejo del contexto.
 * 
 * @author Erwin De León
 */
public interface ContextoUtilityService {
    
    /**
     * Obtiene el contexto del usuario y peticion.
     * @param req ServletRequest 
     * 
     * @return Contexto
     */
    public Contexto obtener(ServletRequest req);            
    
}
