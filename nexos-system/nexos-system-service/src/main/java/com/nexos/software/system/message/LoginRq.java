/*
* Archivo: TemporalRs.java
* Fecha: 08/11/2020
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva de NEXOS SOFTWARE
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de GRUPO NEXOS SOFTWARE
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
*/
package com.nexos.software.system.message;

import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModelProperty;

/**
 * Se duplica logica de colaborador para iniciar sesion en admin web, pero esa
 * esta en core, la que se va a realizar se hara en system, se debe refactorizar
 * luego y borrar lo que se repita, hago esto para no alterar portal colaborador
 * 
 * @author Erwin De León
 */
public class LoginRq {
	@NotBlank
	@ApiModelProperty(notes = "usuarioToken", required = true)
	private String usuarioToken;

	public String getUsuarioToken() {
		return usuarioToken;
	}

	public void setUsuarioToken(String usuarioToken) {
		this.usuarioToken = usuarioToken;
	}

}
