/*
* Archivo: AuditoriaUtilityService.java
* Fecha: 07/11/2020
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva de NEXOS SOFTWARE
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de NEXOS SOFTWARE
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
*/
package com.nexos.software.system.service;

import com.nexos.software.system.entity.AuditoriaEntidad;
import com.nexos.software.system.entity.StandardEntity;
import com.nexos.software.system.model.Contexto;


/**
 * Servicio utilitario para manejo de auditoria de entidades y del sistema.
 * 
 * @author Erwin De León
 */
public interface AuditoriaUtilityService {

    /**
     * Registra en auditoria un evento (creacion, modificacion o eliminacion) de una entidad.
     * @param <T> Entidad que extiende StandardEntity
     * @param contexto Contexto de la peticion.
     * @param entity Entidad
     * @param evento Evento
     */
    <T extends StandardEntity> void registrarEventoEntidad(Contexto contexto, T entity, String evento);
    /**
     * Registrar un log de auditoria del sistema.
     * @param categoria Categoria del log.
     * @param subcategoria Subcategoria del log.
     * @param contexto Contexto del sistema.
     * @param descripcion Contenido del log.
     * @param args Argumentos de la descripcion del log.
     * @para args Argumentos de la descripcion.
     */
    void registrar(Short categoria, Short subcategoria, Contexto contexto, 
            String descripcion, Object... args);
    
    void registrarEventoEntidad(AuditoriaEntidad entity);
    
}
