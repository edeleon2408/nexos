/*
* Archivo: PersonaServiceImpl.java
* Fecha: 07/11/2020
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva de NEXOS SOFTWARE
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de NEXOS SOFTWARE
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
 */
package com.nexos.software.system.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nexos.software.system.entity.AuditoriaEntidad;
import com.nexos.software.system.entity.Persona;
import com.nexos.software.system.message.PersonaDatosRs;
import com.nexos.software.system.model.Contexto;
import com.nexos.software.system.service.AuditoriaUtilityService;
import com.nexos.software.system.service.ConfiguracionUtilityService;
import com.nexos.software.system.service.PersonaService;
import com.nexos.software.system.util.DateUtil;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonaServiceImpl implements PersonaService {

    private final PersonaEntityService personaEntityService;
    
    @Autowired
    private PersonaRepository personaRepository;

    private final Logger logger = LoggerFactory.getLogger(PersonaServiceImpl.class);

    public PersonaServiceImpl(PersonaEntityService personaEntityService) {
        this.personaEntityService = personaEntityService;
    }

    @Override
    public Persona actualizar(Contexto contexto, PersonaDatosRs personaNucleoFamiliarRs) {
        List<Persona> personaLst = new ArrayList<>();
        Persona persona = new Persona();
        persona.setIdentificacion(personaNucleoFamiliarRs.getIdentificacion());
        persona.setPrimerNombre(personaNucleoFamiliarRs.getPrimerNombre());
        persona.setSegundoNombre(personaNucleoFamiliarRs.getSegundoNombre());
        persona.setPrimerApellido(personaNucleoFamiliarRs.getPrimerApellido());
        persona.setSegundoApellido(personaNucleoFamiliarRs.getSegundoApellido());
        persona.setDireccion(personaNucleoFamiliarRs.getDireccion());
        persona.setTelefono(personaNucleoFamiliarRs.getTelefono());
        persona.setEmail(personaNucleoFamiliarRs.getEmail());
        persona.setEstado("activo");
        personaLst.add(persona);
        
        /*Iniciamos a recorrer el nucleo familiar para in gurdandolos*/
        for (int i = 0; i <= personaNucleoFamiliarRs.getNucleoFamiliar().size(); i++) {
            persona.setIdentificacion(personaNucleoFamiliarRs.getNucleoFamiliar().get(i).getIdentificacion());
            persona.setPrimerNombre(personaNucleoFamiliarRs.getNucleoFamiliar().get(i).getPrimerNombre());
            persona.setSegundoNombre(personaNucleoFamiliarRs.getNucleoFamiliar().get(i).getSegundoNombre());
            persona.setPrimerApellido(personaNucleoFamiliarRs.getNucleoFamiliar().get(i).getPrimerApellido());
            persona.setSegundoApellido(personaNucleoFamiliarRs.getNucleoFamiliar().get(i).getSegundoApellido());
            persona.setDireccion(personaNucleoFamiliarRs.getNucleoFamiliar().get(i).getDireccion());
            persona.setTelefono(personaNucleoFamiliarRs.getNucleoFamiliar().get(i).getTelefono());
            persona.setEmail(personaNucleoFamiliarRs.getNucleoFamiliar().get(i).getEmail());
            persona.setEstado("activo");
            personaLst.add(persona);
        }
        personaRepository.saveAll(personaLst);
        
        return persona;
    }

    @Override
    public Persona crear(Contexto contexto, PersonaDatosRs personaNucleoFamiliarRs) {
        List<Persona> personaLst = new ArrayList<>();
        Persona persona = new Persona();
        persona.setIdentificacion(personaNucleoFamiliarRs.getIdentificacion());
        persona.setPrimerNombre(personaNucleoFamiliarRs.getPrimerNombre());
        persona.setSegundoNombre(personaNucleoFamiliarRs.getSegundoNombre());
        persona.setPrimerApellido(personaNucleoFamiliarRs.getPrimerApellido());
        persona.setSegundoApellido(personaNucleoFamiliarRs.getSegundoApellido());
        persona.setDireccion(personaNucleoFamiliarRs.getDireccion());
        persona.setTelefono(personaNucleoFamiliarRs.getTelefono());
        persona.setEmail(personaNucleoFamiliarRs.getEmail());
        persona.setEstado("activo");
        personaLst.add(persona);
        
        /*Iniciamos a recorrer el nucleo familiar para in gurdandolos*/
        for (int i = 0; i <= personaNucleoFamiliarRs.getNucleoFamiliar().size(); i++) {
            persona.setIdentificacion(personaNucleoFamiliarRs.getNucleoFamiliar().get(i).getIdentificacion());
            persona.setPrimerNombre(personaNucleoFamiliarRs.getNucleoFamiliar().get(i).getPrimerNombre());
            persona.setSegundoNombre(personaNucleoFamiliarRs.getNucleoFamiliar().get(i).getSegundoNombre());
            persona.setPrimerApellido(personaNucleoFamiliarRs.getNucleoFamiliar().get(i).getPrimerApellido());
            persona.setSegundoApellido(personaNucleoFamiliarRs.getNucleoFamiliar().get(i).getSegundoApellido());
            persona.setDireccion(personaNucleoFamiliarRs.getNucleoFamiliar().get(i).getDireccion());
            persona.setTelefono(personaNucleoFamiliarRs.getNucleoFamiliar().get(i).getTelefono());
            persona.setEmail(personaNucleoFamiliarRs.getNucleoFamiliar().get(i).getEmail());
            persona.setEstado("activo");
            personaLst.add(persona);
        }
        personaRepository.saveAll(personaLst);
        
        return persona;
    }

    @Override
    public void eliminar(Contexto contexto, PersonaDatosRs personaNucleoFamiliarRs) {
        Persona persona = new Persona();
        this.personaEntityService.crear(contexto, persona);
    }

}
