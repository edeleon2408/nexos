/*
* Archivo: PersonaRepository.java
* Fecha: 08/11/2020
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva de NEXOS SOFTWARE
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de NEXOS SOFTWARE
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
 */
package com.nexos.software.system.service.impl;

import com.nexos.software.system.entity.Persona;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

interface PersonaRepository extends JpaRepository<Persona, String> {

/*    @Query("SELECT p FROM Persona p WHERE p.usuario.id=:idUsuario")
    Persona buscarPorUsuario(@Param("idUsuario") String idUsuario);
*/
    int countByIdentificacion(String identificacion);

    @Query("select p from Persona p where p.estado = :estado")
    public List<Persona> findAllAndEstado(String estado);
    
}
