/*
* Archivo: ConfiguracionUtilityService.java
* Fecha: 07/11/2020
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva de NEXOS SOFTWARE
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de NEXOS SOFTWARE
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
*/
package com.nexos.software.system.service.impl;
 
import com.nexos.software.system.entity.Configuracion;
import com.nexos.software.system.service.ConfiguracionUtilityService;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

/**
 * Implementacion de la interfaz ConfiguracionUtilityService.
 * @author Erwin De León
 */
@Service
class ConfiguracionUtilityServiceImpl implements ConfiguracionUtilityService {

    @Autowired
    private Environment environment;

    @Autowired
    private ConfiguracionRepository configuracionRepository;

    public ConfiguracionUtilityServiceImpl(ConfiguracionRepository configuracionRepository) {
        this.configuracionRepository = configuracionRepository;
    }

    @Override
    public <T> T obtener(String codigo, Class<T> type) {
        Optional<Configuracion> conf = configuracionRepository.findById(codigo);
       
        if (conf.isPresent()) {
            return autocast(conf.get().getValor(), type);
        } else {
            // Tomar valor de .properties
            String val = environment.getProperty(codigo);
            if (val != null) {
                return autocast(val, type);
            } else {
                throw new IllegalArgumentException(String.format("Codigo de configuracion invalido: %s", codigo));
            }
        }
    }

    private <T> T autocast(String valor, Class<T> type) {
        if (type.isInstance("")) {
            return type.cast(valor);
        } else if (type.isInstance(new Integer(0))) {
            return type.cast(Integer.valueOf(valor));
        } else if (type.isInstance(new Long(0))) {
            return type.cast(Long.valueOf(valor));
        } else if (type.isInstance(new Boolean(false))) {
            return type.cast(Boolean.valueOf(valor));
        } else {
            throw new IllegalArgumentException(String.format("Tipo de dato invalido %s", type.toString()));
        }
    }

}
