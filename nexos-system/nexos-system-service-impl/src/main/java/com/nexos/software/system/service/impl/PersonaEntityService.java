/*
* Archivo: PersonaEntityService.java
* Fecha: 07/11/2020
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva de NEXOS SOFTWARE
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de NEXOS SOFTWARE
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
 */
package com.nexos.software.system.service.impl;

import com.nexos.software.system.entity.Persona;
import com.nexos.software.system.message.PersonaDatosRs;
import com.nexos.software.system.model.Contexto;
import com.nexos.software.system.service.PersistenciaUtilityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional; 

@Service
public class PersonaEntityService {

    private final PersonaRepository personaRepository;
    private final PersistenciaUtilityService persistenciaUtilityService;

    private final Logger logger = LoggerFactory.getLogger(PersonaEntityService.class);

    public PersonaEntityService(PersonaRepository personaRepository,
            PersistenciaUtilityService persistenciaUtilityService) {
        this.personaRepository = personaRepository;
        this.persistenciaUtilityService = persistenciaUtilityService;
    }

    @Transactional
    public Persona crear(Contexto contexto, Persona persona) {
        return persistenciaUtilityService.crear(contexto, persona);
    }

    @Transactional
    public Persona actualizar(Contexto contexto, Persona persona) {
        return persistenciaUtilityService.actualizar(contexto, persona);
    }
    
    @Transactional
    public void eliminar(Contexto contexto, Persona persona) {
        persistenciaUtilityService.eliminar(contexto, persona);
    }

    @Transactional(readOnly = true)
    public Iterable<Persona> findAll(String instanciaId) {
        logger.info("findAllByInstanciaId({})", instanciaId);
        return personaRepository.findAll();
    }

}
