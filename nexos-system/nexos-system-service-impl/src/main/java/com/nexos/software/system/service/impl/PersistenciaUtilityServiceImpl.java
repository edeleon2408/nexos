/*
* Archivo: PersistenciaUtilityService.java
* Fecha: 07/11/2020
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva de NEXOS SOFTWARE
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de NEXOS SOFTWARE
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
 */
package com.nexos.software.system.service.impl;

import com.nexos.software.system.entity.StandardEntity;
import com.nexos.software.system.model.Contexto;
import com.nexos.software.system.service.AuditoriaUtilityService;
import com.nexos.software.system.service.ConfiguracionUtilityService;
import com.nexos.software.system.service.PersistenciaUtilityService;
import com.nexos.software.system.util.ConstantesUtil;
import com.nexos.software.system.util.DateUtil;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Implementacion de PersistenciaUtilityService.
 * @author Erwin De León
 */
@Service
public class PersistenciaUtilityServiceImpl implements PersistenciaUtilityService {

    @PersistenceContext
    private EntityManager em;
    @Autowired
    private AuditoriaUtilityService auditoriaUtilityService;
    @Autowired
    private ConfiguracionUtilityService configuracionService;

    @Transactional
    @Override
    public <T extends StandardEntity> T crear(Contexto contexto, T entity) {
        entity.setFechaModificacion(DateUtil.nowDateTime());
        entity = em.merge(entity);
        if (configuracionService.obtener(ConstantesUtil.SYS_AUDITORIA_ENTIDADES, Boolean.class)) {
            auditoriaUtilityService.registrarEventoEntidad(contexto, entity, AuditoriaUtilityServiceImpl.CREATE);
        }
        return entity;
    }

    @Transactional
    @Override
    public <T extends StandardEntity> T actualizar(Contexto contexto, T entity) {
        entity.setFechaModificacion(DateUtil.nowDateTime());
        entity = em.merge(entity);
        if (configuracionService.obtener(ConstantesUtil.SYS_AUDITORIA_ENTIDADES, Boolean.class)) {
            auditoriaUtilityService.registrarEventoEntidad(contexto, entity, AuditoriaUtilityServiceImpl.UPDATE);
        }
        return entity;
    }

    @Transactional
    @Override
    public <T extends StandardEntity> void eliminar(Contexto contexto, T entity) {
        if (configuracionService.obtener(ConstantesUtil.SYS_AUDITORIA_ENTIDADES, Boolean.class)) {
            auditoriaUtilityService.registrarEventoEntidad(contexto, entity, AuditoriaUtilityServiceImpl.DELETE);
        }
        em.remove(entity);
    }

}
