/*
* Archivo: AuditoriaUtilityService.java
* Fecha: 07/11/2020
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva de NEXOS SOFTWARE
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de NEXOS SOFTWARE
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
 */
package com.nexos.software.system.service.impl;

import java.time.LocalDateTime;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper; 
import com.nexos.software.system.entity.AuditoriaEntidad;
import com.nexos.software.system.entity.AuditoriaSistema;
import com.nexos.software.system.entity.StandardEntity;
import com.nexos.software.system.model.Contexto;
import com.nexos.software.system.service.AuditoriaUtilityService;
import com.nexos.software.system.service.ConfiguracionUtilityService;
import com.nexos.software.system.util.ConstantesUtil;
import com.nexos.software.system.util.DateUtil;

@Service
class AuditoriaUtilityServiceImpl implements AuditoriaUtilityService {

    static final String CREATE = "C";
    static final String UPDATE = "U";
    static final String DELETE = "D";

    private final AuditoriaEntidadRepository auditoriaEntidadRepository;
    private final ConfiguracionUtilityService configuracionUtilityService;
    private final AuditoriaSistemaRepository auditoriaSistemaRepository;

    public AuditoriaUtilityServiceImpl(AuditoriaEntidadRepository auditoriaEntidadRepository,
            ConfiguracionUtilityService configuracionUtilityService, 
            AuditoriaSistemaRepository auditoriaSistemaRepository) {
        this.auditoriaEntidadRepository = auditoriaEntidadRepository;
        this.configuracionUtilityService = configuracionUtilityService;
        this.auditoriaSistemaRepository = auditoriaSistemaRepository;
    }

    @Override
    public <T extends StandardEntity> void registrarEventoEntidad(Contexto contexto, T entity, String evento) {
        if (configuracionUtilityService.obtener(ConstantesUtil.SYS_AUDITORIA_ENTIDADES, Boolean.class)) {
            auditoriaEntidadRepository.save(generarAuditoriaEntidad(contexto, entity, evento));
        }

    }

    private AuditoriaEntidad generarAuditoriaEntidad(Contexto contexto, StandardEntity entity, String evento) {
        try {
            AuditoriaEntidad aud = new AuditoriaEntidad();
            aud.setEntidad(entity.getClass().getName());
            aud.setEntidadId(entity.getId());
            aud.setEvento(evento);
            aud.setFecha(DateUtil.nowDateTime());
            aud.setUsuarioId("null");
            aud.setFecha(DateUtil.nowDateTime());
            ObjectMapper mapper = new ObjectMapper();
            aud.setSnapshot(mapper.writeValueAsString(entity));
            aud.setUsuarioId(contexto.get(Contexto.USUARIO_ID));
            return aud;
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    @Transactional
    public void registrar(Short categoria, Short subcategoria, Contexto contexto,
            String descripcion, Object... args) {
        LocalDateTime now = DateUtil.nowDateTime();
        String desc = String.format(descripcion, args);
        AuditoriaSistema aud = new AuditoriaSistema(categoria, subcategoria, contexto.get(Contexto.CLIENTE_IP),
                desc, now, contexto.get(Contexto.EMPRESA_ID), contexto.get(Contexto.USUARIO_ID));
        auditoriaSistemaRepository.save(aud);
    }

	@Override
	public void registrarEventoEntidad(AuditoriaEntidad entity) {
		auditoriaEntidadRepository.save(entity);
	}

}
