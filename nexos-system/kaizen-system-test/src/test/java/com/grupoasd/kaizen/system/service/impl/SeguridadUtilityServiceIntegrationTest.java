/*
* Archivo: SeguridadUtilityServiceIntegrationTest.java
* Fecha: 26/01/2019
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva del GRUPO ASESORIA EN
* SISTEMATIZACION DE DATOS SOCIEDAD POR ACCIONES SIMPLIFICADA GRUPO ASD
  S.A.S.
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de GRUPO ASD S.A.S.
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
 */
package com.grupoasd.kaizen.system.service.impl;

import com.grupoasd.kaizen.Application;
import com.grupoasd.kaizen.system.util.SecurityUtil;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.Base64;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import static org.junit.Assert.*;

/**
 * Prueba de integracion de SeguridadUtilityService.
 *
 * @author Juan Carlos Castellanos jccastellanos@grupoasd.com.co
 */
@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = Application.class
)
public class SeguridadUtilityServiceIntegrationTest {

    @Value("${sys.login.secret}")
    private String secret;
    @Value("${sys.login.vector}")
    private String vector;

    @Autowired
    private SeguridadUtilityService seguridadUtilityService;

    @Test
    public void obtenerUsuarioDeTokenTest() {
        String usuario = "jccastellanos@grupoasd.com.co";
        String clave = "qazwsx123456";
        System.out.println("----------" + secret);
        String textoEncriptado = "";
        try {
            textoEncriptado = SecurityUtil.encrypt(secret, vector, (String.format("%s:%s", usuario, clave)));
            assertNotNull(textoEncriptado);
            System.out.println("TextoEncriptado: " + textoEncriptado);
            System.out.println("**UsuarioDes:" + seguridadUtilityService.obtenerUsuarioDeToken(secret, vector, textoEncriptado));
            System.out.println("**ClaveDes:" + seguridadUtilityService.obtenerClaveDeToken(secret, vector, textoEncriptado));

        } catch (Exception ex) {
            Logger.getLogger(SeguridadUtilityServiceIntegrationTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
