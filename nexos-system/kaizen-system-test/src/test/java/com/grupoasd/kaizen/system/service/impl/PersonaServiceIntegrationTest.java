/*
* Archivo: UsuarioServiceIntegrationTest.java
* Fecha: 22/01/2019
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva del GRUPO ASESORIA EN
* SISTEMATIZACION DE DATOS SOCIEDAD POR ACCIONES SIMPLIFICADA GRUPO ASD
  S.A.S.
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de GRUPO ASD S.A.S.
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
 */
package com.grupoasd.kaizen.system.service.impl;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Optional;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.grupoasd.kaizen.Application;
import com.grupoasd.kaizen.system.entity.Persona;
import com.grupoasd.kaizen.system.entity.Usuario;
import com.grupoasd.kaizen.system.message.PersonaActualizarRq;
import com.grupoasd.kaizen.system.message.UsuarioCrearRq;
import com.grupoasd.kaizen.system.model.Contexto;
import com.grupoasd.kaizen.system.model.GenericRs;
import com.grupoasd.kaizen.system.service.PersonaService;
import com.grupoasd.kaizen.system.service.UsuarioService;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = Application.class
)
public class PersonaServiceIntegrationTest {

    @Autowired
    PersonaService personaService;
    @Autowired
    UsuarioRepository usuarioRepository;
    @Autowired
    PersonaRepository personaRepository;

    private Optional<Usuario> usuario;
    private Persona persona;

    private Contexto getContexto() {
        Contexto ctx = new Contexto();
   
        return ctx;
    }

    @Test
    public void testActualizar() {

        Persona persona=this.personaRepository.buscarPorId("anonimo");
          System.out.println(persona.getId());
        PersonaActualizarRq rq = new PersonaActualizarRq();
        rq.setId(persona.getId());
        rq.setBarrio("Marly");
        rq.setCelular("3156985452");
        rq.setDireccion("Av 11 E N. 6-45");
        rq.setSegundoApellido("Ortiz");
        Contexto contexto = ContextoUtil.obtener();
        System.out.println(contexto);
        GenericRs rs=personaService.actualizar(contexto, rq);
        assertNotNull(rs);
	assertEquals(1, rs.getCodigo()); 

    }

    @After
    public void after() {

    }

}
