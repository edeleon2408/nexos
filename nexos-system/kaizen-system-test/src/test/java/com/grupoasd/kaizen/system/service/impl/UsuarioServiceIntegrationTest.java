/*
* Archivo: UsuarioServiceIntegrationTest.java
* Fecha: 22/01/2019
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva del GRUPO ASESORIA EN
* SISTEMATIZACION DE DATOS SOCIEDAD POR ACCIONES SIMPLIFICADA GRUPO ASD
  S.A.S.
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de GRUPO ASD S.A.S.
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
 */
package com.grupoasd.kaizen.system.service.impl;
 
import static org.junit.Assert.*;

import java.util.List;
import java.util.Optional;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.grupoasd.kaizen.Application;
import com.grupoasd.kaizen.system.entity.Persona;
import com.grupoasd.kaizen.system.entity.Usuario;
import com.grupoasd.kaizen.system.message.UsuarioCrearRq;
import com.grupoasd.kaizen.system.model.Contexto;
import com.grupoasd.kaizen.system.model.GenericRs;
import com.grupoasd.kaizen.system.service.UsuarioService;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = Application.class
)
public class UsuarioServiceIntegrationTest {

    @Autowired
    UsuarioService usuarioService;
    @Autowired
    UsuarioRepository usuarioRepository;
    @Autowired
    PersonaRepository personaRepository;

    private Optional<Usuario> usuario;
    private Persona persona;

    private Contexto getContexto() {
        Contexto ctx = new Contexto();
        ctx.put(Contexto.USUARIO_ID, "anonimo");
        return ctx;
    }

    @Test
    public void testCrear() {
        String login = "juancastellanosm@gmail.com";
        String nombre1 = "Juan";
        String nombre2 = "Carlos";
        String apellido1 = "Castellanos";
        String apellido2 = "Moreno";
        String tipoDoc = "cc";
        String documento = "654321";
        String email = "juancastellanosm@gmail.com";
        String movil = "3000000000";
        UsuarioCrearRq rq = new UsuarioCrearRq();
        rq.setUsuario(login);
        rq.setClave("123456");
        rq.setTipoDocumento(tipoDoc);
        rq.setDocumento(documento);
        rq.setEmail(email);
        rq.setPrimerNombre(nombre1);
        rq.setSegundoNombre(nombre2);
        rq.setPrimerApellido(apellido1);
        rq.setSegundoApellido(apellido2);
        rq.setMovil(movil);
        GenericRs rs = usuarioService.crear(getContexto(), rq);
        assertNotNull(rs);
        assertEquals(1, rs.getCodigo());
        usuario = usuarioRepository.findByUsuario(login);
        assertTrue(usuario.isPresent());
        assertEquals(movil, usuario.get().getMovil());
        //List<Persona> personas = personaRepository.findByUsuario(usuario.get());
        //assertEquals(1, personas.size());
        persona = personaRepository.buscarPorUsuario(usuario.get().getId());
        assertEquals(nombre1, persona.getPrimerNombre());
        assertEquals(nombre2, persona.getSegundoNombre());
        assertEquals(apellido1, persona.getPrimerApellido());
        assertEquals(apellido2, persona.getSegundoApellido());
    }

    @After
    public void after() {
        if (persona != null) {
            personaRepository.delete(persona);
        }
        if (usuario.isPresent()) {
            usuarioRepository.delete(usuario.get());
        }
    }

}
