/*
* Archivo: UsuarioServiceIntegrationTest.java
* Fecha: 22/01/2019
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva del GRUPO ASESORIA EN
* SISTEMATIZACION DE DATOS SOCIEDAD POR ACCIONES SIMPLIFICADA GRUPO ASD
  S.A.S.
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de GRUPO ASD S.A.S.
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
 */
package com.grupoasd.kaizen.system.service.impl;
 
import com.grupoasd.kaizen.Application;
import com.grupoasd.kaizen.security.JwtTokenProvider;
import com.grupoasd.kaizen.system.entity.Usuario;
import java.util.Optional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import static org.junit.Assert.*;

/**
 * JwtTokenProvider.
 * 
 * @author Juan Carlos Castellanos jccastellanos@grupoasd.com.co
 */
@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = Application.class
)
public class JwtTokenProviderIntegrationTest {
    
    @Autowired
    private JwtTokenProvider jwtTokenProvider;
    @Autowired
    private UsuarioRepository usuarioRepository;
    
    @Test
    public void testGenerar() {
        Optional<Usuario> usuario = usuarioRepository.findByUsuario("azuluaga@grupoasd.com.co");
        String token = jwtTokenProvider.generar(usuario.get(), "2354897456fgdr698756985dersdf2365");
        System.out.println("Token: " + token);
        assertNotNull(token);
    }
    
}
