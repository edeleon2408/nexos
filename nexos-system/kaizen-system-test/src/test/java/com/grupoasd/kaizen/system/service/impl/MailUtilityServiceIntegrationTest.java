/*
* Archivo: MailUtilityServiceIntegrationTest.java
* Fecha: 23/01/2019
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva del GRUPO ASESORIA EN
* SISTEMATIZACION DE DATOS SOCIEDAD POR ACCIONES SIMPLIFICADA GRUPO ASD
  S.A.S.
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de GRUPO ASD S.A.S.
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
*/
package com.grupoasd.kaizen.system.service.impl;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import com.grupoasd.kaizen.Application;
import com.grupoasd.kaizen.system.model.Mail;
import com.grupoasd.kaizen.system.model.Validacion;
import com.grupoasd.kaizen.system.service.MailUtilityService;
import com.grupoasd.kaizen.system.service.TemplateUtilityService;
import com.grupoasd.kaizen.system.util.ConstantesUtil;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = Application.class
)
public class MailUtilityServiceIntegrationTest {
    
    @Autowired
    private MailUtilityService mailUtilityService;
    @Autowired
    private TemplateUtilityService templateUtilityService;
    
    //@Test
    public void testSendText() {     
        try {
            // Mail mail = new Mail("juancastellanosm@gmail.com", "Prueba de correo", "Esta es una prueba", false);
            Mail mail = new Mail("edeleon2408@gmail.com", "Prueba de correo", "Esta es una prueba", false);
            mailUtilityService.send(mail,"");
            assertTrue(true);
        } catch (MessagingException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    @Test
    public void testSendHtml() {
        Map<String, Object> model = new HashMap<>();
        //model.put("usuario", "juancastellanosm@gmail.com");
        model.put("usuario", "edeleon@grupoasd.com.co");
        model.put("primerNombre", "Erwin");
        model.put("primerApellido", "De Leon");
        model.put("correo", "edeleon@grupoasd.com.co");
        model.put("registro_id", "e2bc437b280d4a48342a4ec02a5254af");
        model.put("url_mail", "http://localhost:4200/confirmar-registro");
        String cuerpo = templateUtilityService.processTemplateToString(
                ConstantesUtil.PLT_CORREO_REG_COLABORADOR, model, ConstantesUtil.CARPETA_PLANTILLAS);
        Mail mail = new Mail("juancastellanosm@gmail.com", "Prueba de registro colaborador", cuerpo, true);
        try {
            mailUtilityService.send(mail, "registro");
        } catch (MessagingException ex) {
            throw new RuntimeException(ex);
        }
        assertTrue(true);
    }
}
