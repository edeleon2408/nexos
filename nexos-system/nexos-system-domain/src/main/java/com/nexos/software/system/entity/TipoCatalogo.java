/*
* Archivo: TipoCatalogo.java
* Fecha: 22/01/2019
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva de NEXOS SOFTWARE
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de NEXOS SOFTWARE
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
 */
package com.nexos.software.system.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;

/**
 * Entidad de negocio que representa un tipo de catalogo.
 *
 * @author Erwin De León
 */
@Entity
@Table(name = "sys_tipo_catalogo", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"nombre"}),})
public class TipoCatalogo extends StandardEntity {

    private static final long serialVersionUID = -2646030269002744887L;

    @Id
    @Column(name = "tipo_catalogo_id", length = 40)
    @JsonView({VistaSystemRs.TipoCatalogo.class})
    private String id;
    
    @Column(name = "nombre", nullable = false, length = 100)
    private String nombre;
    
    @JsonIgnoreProperties("tipoCatalogo")
    @OneToMany(mappedBy = "tipoCatalogo")
    @JsonView({VistaSystemRs.TipoCatalogo.class})
    private List<Catalogo> catalogos;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Catalogo> getCatalogos() {
        return catalogos;
    }

    public void setCatalogos(List<Catalogo> catalogos) {
        this.catalogos = catalogos;
    }

}
