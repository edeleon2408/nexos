/*
* Archivo: Token.java
* Fecha: 07/11/2020
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva de NEXOS SOFTWARE
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de NEXOS SOFTWARE
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
*/
package com.nexos.software.system.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

/**
 * Entidad de negocio Token.
 * @author Erwin De León
 */
@Entity
@Table(name = "sys_token")
public class Token implements Serializable {   
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "token_id", length = 40)
    private String id;
    
    /**
     * Identificador del usuario.    
     * No se hace uso de la referencia utilizando JPA para 
     * evitar joins innecesarios.
     */
    @Column(name = "usuario_id", nullable = false, length = 40)
    private String usuarioId;
    /**
     * Token JWT.
     */
    @Lob
    @Column(name = "codigo", nullable = false)
    private String codigo;
    /**
     * Fecha de creacion del token.
     */
    @Column(name = "fecha_creacion", nullable = false)
    private LocalDateTime fechaCreacion;
    /**
     * Fecha de la ultima actividad del usuario.
     */
    @Column(name = "fecha_ultima_actividad", nullable = false)
    private LocalDateTime fechaUltimaActividad;
    
    /**
     * estado del token para un usuario.
     */
    @Column(name = "estado", nullable = false)
    private boolean estado;
    

    public Token() {
        
    }        

    public Token(String id, String usuarioId, String codigo, LocalDateTime fechaCreacion,
            LocalDateTime fechaUltimaActividad, boolean estado) {
        this.id = id;
        this.usuarioId = usuarioId;
        this.codigo = codigo;
        this.fechaCreacion = fechaCreacion;
        this.fechaUltimaActividad = fechaUltimaActividad;
        this.estado = estado;
    }    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(String usuarioId) {
        this.usuarioId = usuarioId;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public LocalDateTime getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(LocalDateTime fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public LocalDateTime getFechaUltimaActividad() {
        return fechaUltimaActividad;
    }

    public void setFechaUltimaActividad(LocalDateTime fechaUltimaActividad) {
        this.fechaUltimaActividad = fechaUltimaActividad;
    }
        
     public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
}
