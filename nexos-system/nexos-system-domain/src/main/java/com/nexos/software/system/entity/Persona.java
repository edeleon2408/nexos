/*
* Archivo: Persona.java
* Fecha: 07/11/2020
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva de NEXOS SOFTWARE
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de NEXOS SOFTWARE
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
*/
package com.nexos.software.system.entity;

import com.nexos.software.system.util.UuidProvider;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;


@Entity
@Table(name = "sys_persona")
/*@NamedQueries({
    @NamedQuery(name = "Persona.buscarPorId", query = "SELECT p FROM Persona p WHERE p.id = :id "),
    @NamedQuery(name = "Persona.buscarPorTelefonoId", query = "SELECT p FROM Persona p WHERE p.telefono=:celular and p.id=:id")
})*/
public class Persona extends StandardEntity {

    private static final long serialVersionUID = -824889346290521457L;

    @Id
    @Column(name = "persona_id", length = 40)
    private String id;

    /*@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "catalogo_tipo_identificacion_id", nullable = true)
    private Catalogo tipoIdentificacion;*/
    
    @Column(name = "identificacion", nullable = true, length = 30)
    private String identificacion;
    
    @Column(name = "primer_nombre", nullable = true, length = 50)
    private String primerNombre;
    
    @Column(name = "segundo_nombre", nullable = true, length = 50)
    private String segundoNombre;
    
    @Column(name = "primer_apellido", nullable = true, length = 50)
    private String primerApellido;
    
    @Column(name = "segundo_apellido", nullable = true, length = 50)
    private String segundoApellido;
    
    @Column(name = "direccion", nullable = true, length = 300)
    private String direccion;
    
    @Column(name = "telefono")
    private int telefono;

    @Column(name = "email", nullable = true, length = 150)
    private String email;
    
    @DateTimeFormat(iso = ISO.DATE)
    @Column(name = "fecha_nacimiento", nullable = true)
    private LocalDate fechaNacimiento;
   
    // Activo o inactivo
    @Column(name = "estado", nullable = true, length = 15)
    private String estado;
    
  
    public Persona() {
        this.id = UuidProvider.createUuid();
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

   /* public Catalogo getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(Catalogo tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }*/

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    
}
