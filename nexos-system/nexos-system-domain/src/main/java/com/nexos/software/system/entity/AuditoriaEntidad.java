/*
* Archivo: AuditoriaEntidad.java
* Fecha: 07/11/2020
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva de NEXOS SOFTWARE
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de NEXOS SOFTWARE
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
 */
package com.nexos.software.system.entity;

import com.nexos.software.system.util.UuidProvider;
import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Version;


/**
 * Entidad que representa la auditoria de una entidad.
 *
 * @author Erwin De León
 */
@Entity
@Table(name = "sys_auditoria_entidad")
public class AuditoriaEntidad implements Serializable {

    private static final long serialVersionUID = -616243869369786784L;

    /**
     * Identificador de la entidad.
     */
    @Id
    @Column(name = "id", length = 40)
    private String id;
    /**
     * Version JPA de la entidad.
     */
    @Version
    @Column(name = "version", nullable = false)
    private Integer version;
    /**
     * Fecha de auditoria.
     */
    @Column(name = "fecha", nullable = false)
    private LocalDateTime fecha;
    @Column(name = "evento", nullable = false, columnDefinition = "CHAR(1)")
    private String evento;
    @Column(name = "usuario_id", nullable = false, length = 32)
    private String usuarioId;
    @Column(name = "entidad", nullable = false, length = 100)
    private String entidad;
    @Column(name = "entidad_id", nullable = false, length = 32)
    private String entidadId;
    @Lob
    @Column(name = "snapshot", nullable = true)
    private String snapshot;

    public AuditoriaEntidad() {
        id = UuidProvider.createUuid();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public LocalDateTime getFecha() {
        return fecha;
    }

    public void setFecha(LocalDateTime fecha) {
        this.fecha = fecha;
    }

    public String getEvento() {
        return evento;
    }

    public void setEvento(String evento) {
        this.evento = evento;
    }

    public String getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(String usuarioId) {
        this.usuarioId = usuarioId;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public String getEntidadId() {
        return entidadId;
    }

    public void setEntidadId(String entidadId) {
        this.entidadId = entidadId;
    }

    public String getSnapshot() {
        return snapshot;
    }

    public void setSnapshot(String snapshot) {
        this.snapshot = snapshot;
    }

}
