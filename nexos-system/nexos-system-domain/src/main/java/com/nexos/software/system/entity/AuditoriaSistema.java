/*
* Archivo: AuditoriaSistema.java
* Fecha: 07/11/2020
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva de NEXOS SOFTWARE
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de NEXOS SOFTWARE
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
*/
package com.nexos.software.system.entity;

import com.nexos.software.system.util.UuidProvider;
import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * Entidad que representa la auditoria del sistema.
 *
 * @author Erwin De León
 */
@Entity
@Table(name = "sys_auditoria_sistema")
public class AuditoriaSistema implements Serializable {

    private static final long serialVersionUID = -2166764873889842874L;

    /**
     * Identificador de la entidad.
     */
    @Id
    @Column(name = "id", length = 32)
    private String id;
    /**
     * Version JPA de la entidad.
     */
    @Version
    @Column(name = "version", nullable = false)
    protected Integer version;
    /**
     * Categoria de la auditoria.
     */
    @Column(name = "categoria", nullable = false)
    private Short categoria;
    /**
     * Categoria de la auditoria.
     */
    @Column(name = "subcategoria", nullable = false)
    private Short subcategoria;
    /**
     * IP del cliente.
     */
    @Column(name = "cliente_ip", nullable = true, length = 50)
    private String clienteIp;
    /**
     * Descripcion (log) del movimiento.
     */
    @Lob
    @Column(name = "mensaje", nullable = false)
    private String mensaje;
    /**
     * Fecha de auditoria.
     */
    @Column(name = "fecha", nullable = false)
    private LocalDateTime fecha;

    @Column(name = "empresa_id", nullable = true, length = 32)
    private String empresaId;
    /**
     * Funcionario loggeado registra el movimiento. Por optimizacion no se
     * utiliza mapeo de JPA.
     */
    @Column(name = "usuario_id", nullable = false, length = 36)
    private String usuarioId;

    public AuditoriaSistema() {
        this.id = UuidProvider.createUuid();
    }

    public AuditoriaSistema(Short categoria, Short subcategoria, String clienteIp, String mensaje,
            LocalDateTime fecha, String empresaId, String usuarioId) {
        this.id = UuidProvider.createUuid();
        this.categoria = categoria;
        this.subcategoria = subcategoria;
        this.clienteIp = clienteIp;
        this.mensaje = mensaje;
        this.fecha = fecha;
        this.empresaId = empresaId;
        this.usuarioId = usuarioId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Short getCategoria() {
        return categoria;
    }

    public void setCategoria(Short categoria) {
        this.categoria = categoria;
    }

    public Short getSubcategoria() {
        return subcategoria;
    }

    public void setSubcategoria(Short subcategoria) {
        this.subcategoria = subcategoria;
    }

    public String getClienteIp() {
        return clienteIp;
    }

    public void setClienteIp(String clienteIp) {
        this.clienteIp = clienteIp;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public LocalDateTime getFecha() {
        return fecha;
    }

    public void setFecha(LocalDateTime fecha) {
        this.fecha = fecha;
    }

    public String getEmpresaId() {
        return empresaId;
    }

    public void setEmpresaId(String empresaId) {
        this.empresaId = empresaId;
    }

    public String getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(String usuarioId) {
        this.usuarioId = usuarioId;
    }
}
