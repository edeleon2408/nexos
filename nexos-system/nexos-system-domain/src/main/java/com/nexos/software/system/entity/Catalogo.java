/*
* Archivo: Catalogo.java
* Fecha: 07/11/2020
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva de NEXOS SOFTWARE
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de NEXOS SOFTWARE
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
*/
package com.nexos.software.system.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;

/**
 * Entidad de negocio que representa un catalogo.
 *
 * @author Erwin De León
 */
@Entity
@Table(name = "sys_catalogo")
/*@NamedQueries({
    @NamedQuery(name = "Catalogo.listarPorTipoCatalogo", query = "SELECT c FROM Catalogo c where c.tipoCatalogo.id=:idTipoCatalogo"),
    @NamedQuery(name = "Catalogo.buscarPorTipoYCodigo", query = "SELECT c FROM Catalogo c where c.tipoCatalogo.id = :idTipoCatalogo AND c.codigo = :codigo")})*/
public class Catalogo extends StandardEntity {

    private static final long serialVersionUID = 362417756676792033L;

    @Id
    @Column(name = "catalogo_id", length = 40)
    private String id;
    
    @Column(name = "codigo", nullable = false, length = 32)
    private String codigo;
    
    @Column(name = "nombre", nullable = false, length = 200)
    private String nombre;
    
    @Column(name = "descripcion", nullable = true, length = 300)
    private String descripcion;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "tipo_catalogo_id", nullable = false)
    @JsonIgnore
    private TipoCatalogo tipoCatalogo;
    
    @Column(name = "empresa_id", nullable = false, length = 32)
    private String empresaId;

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public void setId(String id) {
        this.id = id;

    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public TipoCatalogo getTipoCatalogo() {
        return tipoCatalogo;
    }

    public void setTipoCatalogo(TipoCatalogo tipoCatalogo) {
        this.tipoCatalogo = tipoCatalogo;
    }

    public String getEmpresaId() {
        return empresaId;
    }

    public void setEmpresaId(String empresaId) {
        this.empresaId = empresaId;
    }

}
