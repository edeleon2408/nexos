/*
* Archivo: StandardEntity.java
* Fecha: 07/11/2020
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva de NEXOS SOFTWARE
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de NEXOS SOFTWARE
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
 */
package com.nexos.software.system.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonView;

/**
 * Entidad persistente estandar.
 *
 * @author Erwin De León
 */
@MappedSuperclass
public abstract class StandardEntity implements Serializable {

    private static final long serialVersionUID = -2956929553833874711L;
    /**
     * Version JPA de la entidad.
     */
    //@Version
    @Column(name = "version", nullable = false)
    protected Integer version;
    /**
     * Ultima fecha de modificacion. Si la entidad es nueva, almacena la fecha
     * de creacion.
     */
    @Version
    @JsonView(VistaSystemRs.StandardEntity.class)
    @Column(name = "fecha_modificacion", nullable = false)
    protected LocalDateTime fechaModificacion;

    public StandardEntity() {
		this.version = 1;
	}

	public abstract String getId();

    public abstract void setId(String id);

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public LocalDateTime getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(LocalDateTime fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.getId());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final StandardEntity other = (StandardEntity) obj;
        if(!Objects.equals(this.getId(), other.getId())) {
            return false;
        }
        return true;
    }

}
