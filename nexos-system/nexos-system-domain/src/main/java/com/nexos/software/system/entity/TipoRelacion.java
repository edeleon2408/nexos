/*
* Archivo: TipoRelacion.java
* Fecha: 07/11/2020
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva de NEXOS SOFTWARE
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de NEXOS SOFTWARE
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
*/
package com.nexos.software.system.entity;
 
import com.nexos.software.system.util.UuidProvider;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Erwin De León
 */
@Entity
@Table(name = "sys_tipo_relacion")
public class TipoRelacion {
    private static final long serialVersionUID = -824889346290521457L;

    @Id
    @Column(name = "tipo_relacion_id", length = 32)
    private String id;
    
    @Column(name = "nombre_relacion", nullable = true, length = 50)
    private String nombreRelacion;
    
    @Column(name = "descripcion", nullable = true, length = 150)
    private String descripcion;
    
    @Column(name = "estado", nullable = true, length = 10)
    private String estado;
    
    public TipoRelacion() {
        this.id = UuidProvider.createUuid();
    }

    public String getNombreRelacion() {
        return nombreRelacion;
    }

    public void setNombreRelacion(String nombreRelacion) {
        this.nombreRelacion = nombreRelacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    
}
