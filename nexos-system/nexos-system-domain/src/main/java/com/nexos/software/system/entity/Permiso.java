/*
* Archivo: Permiso.java
* Fecha: 07/11/2020
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva de NEXOS SOFTWARE
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de NEXOS SOFTWARE
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
*/
package com.nexos.software.system.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

/**
 * Entidad de negocio que representa un recurso del sistema en un control de
 * accesos basado en RBAC. Un recurso del sistema es una funcionalidad que
 * permite el sistema.
 *
 * @author Erwin De León
 */
@Entity
@Table(name = "sys_permiso")
public class Permiso extends StandardEntity {

    private static final long serialVersionUID = -244933908267201379L;

    @Id
    @Column(name = "permiso_id", length = 40)
    private String id;
    @Column(name = "nombre", nullable = false, length = 255)
    private String nombre;
    @Lob
    @Column(name = "descripcion", nullable = true)
    private String descripcion;
//    @ManyToMany(mappedBy = "permisos", fetch = FetchType.LAZY)
//    private Set<Rol> roles;

    public Permiso() {
        super();
        // El id debe asignarse manualmente por lo cual se coloca en null
        this.id = null;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

//    public Set<Rol> getRoles() {
//        return roles;
//    }
//
//    public void setRoles(Set<Rol> roles) {
//        this.roles = roles;
//    }

}
