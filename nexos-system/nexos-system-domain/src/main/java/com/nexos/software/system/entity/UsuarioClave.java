/*
* Archivo: UsuarioClave.java
* Fecha: 07/11/2020
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva de NEXOS SOFTWARE
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de NEXOS SOFTWARE
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
*/
package com.nexos.software.system.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Almacena las claves antiguas del usuario.
 * @author Erwin De León
 *
 */
@Entity
@Table(name = "sys_usuario_clave")
public class UsuarioClave implements Serializable {

	private static final long serialVersionUID = 1362980360826484633L;
	
	/**
	 * Identificador de la entidad.
	 */
	@Id
    @Column(name = "id", length = 32)
    private String id;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="usuario_id", nullable = false)	
    private Usuario usuario;
	@Column(name = "clave", nullable = false, length = 100)
    private String clave;
	@Column(name = "fecha", nullable = false)
    private LocalDateTime fecha;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public LocalDateTime getFecha() {
		return fecha;
	}
	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}
	
	
}
