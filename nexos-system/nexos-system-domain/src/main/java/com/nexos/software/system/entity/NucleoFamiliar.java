/*
* Archivo: NucleoFamiliar.java
* Fecha: 07/11/2020
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva de NEXOS SOFTWARE
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de NEXOS SOFTWARE
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
*/
package com.nexos.software.system.entity;

import com.nexos.software.system.util.UuidProvider;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Erwin De León
 */
@Entity
@Table(name = "sys_nucleo_familiar")
public class NucleoFamiliar {
    
    private static final long serialVersionUID = -824889346290521457L;

    @Id
    @Column(name = "nucle_familiar_id", length = 32)
    private String id;
    
    @Column(name = "persona_id", nullable = true, length = 32)
    private String personaId;
    
    @Column(name = "relacion_id", nullable = true, length = 32)
    private String relacionId;
    
     public NucleoFamiliar() {
        this.id = UuidProvider.createUuid();
    }

    public String getPersonaId() {
        return personaId;
    }

    public void setPersonaId(String personaId) {
        this.personaId = personaId;
    }

    public String getRelacionId() {
        return relacionId;
    }

    public void setRelacionId(String relacionId) {
        this.relacionId = relacionId;
    }

     
}
