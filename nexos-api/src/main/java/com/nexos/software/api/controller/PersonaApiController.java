/*
* Archivo: PersoanApiController.java
* Fecha: 07/11/2020
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva de Nexus Software
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de Nexus Software
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
 */
package com.nexos.software.api.controller;

import com.nexos.software.api.util.ContextoUtil;
import com.nexos.software.system.entity.Persona;
import com.nexos.software.system.message.PersonaDatosRs; 
import com.nexos.software.system.model.Contexto; 
import com.nexos.software.system.service.PersonaService; 
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.ArrayList;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * Endpoint REST para gestion de personas.
 *
 * @author Erwin De León edeleon2408@gmail.com
 */
@RestController
@RequestMapping("/personas")
@Api(value = "/personas", description = "Gestion de personas.")
public class PersonaApiController {

    private PersonaService personaService;
    
    //@Autowired
    //private PersonaRepository personaRepository;

    public PersonaApiController(PersonaService personaService) {
        this.personaService = personaService;
    }

    /**
     * Registra una persona nueva
     *
     * @param personaNucleoFamiliarRs
     * @param response
     * @return
     */
    @ApiOperation(value = "Registra una persona nueva en el sistema.", response = Persona.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Respuesta del registro de persona donde el atributo codigo representa:"
                + "1. Persona creada satisfactoriamente.\r\n"
                + "2. Ya existe una persona registrada con los datos.\r\n"
                + "3. Error al registrar persona.")})
    @PostMapping("/registrar-persona")
    public ResponseEntity<Persona> registrarPersona(@RequestBody @Validated PersonaDatosRs personaNucleoFamiliarRs,
            HttpServletResponse response) {
        Contexto ctx = ContextoUtil.obtener();
        return ResponseEntity.ok(personaService.crear(ctx, personaNucleoFamiliarRs));

    }

    /**
     * Actualiza una persona
     *
     * @param personaNucleoFamiliarRs
     * @param response
     * @return
     */
    @ApiOperation(value = "Actualiza una persona en el sistema.", response = Persona.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Respuesta de la actualización de persona donde el atributo codigo representa:"
                + "1. Persona actualizada satisfactoriamente.\r\n"
                + "2. No existe una persona registrada con los datos.\r\n"
                + "3. Error al actualizar persona.")})
    @PostMapping("/actualizar-persona")
    public ResponseEntity<Persona> actualizarPersona(@RequestBody @Validated PersonaDatosRs personaNucleoFamiliarRs,
            HttpServletResponse response) {
        Contexto ctx = ContextoUtil.obtener();
        return ResponseEntity.ok(personaService.actualizar(ctx, personaNucleoFamiliarRs));

    }
    
    /**
     * Elimina una persona
     *
     * @param personaNucleoFamiliarRs
     * @param response
     * @return
     */
    @ApiOperation(value = "Elimina una persona del sistema.", response = Persona.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Respuesta de la eliminación de persona donde el atributo codigo representa:"
                + "1. Persona eliminada satisfactoriamente.\r\n"
                + "2. No existe una persona registrada con los datos.\r\n"
                + "3. Error al eliminar persona.")})
    @PostMapping("/eliminar-persona")
    public ResponseEntity<String> eliminarPersona(@RequestBody @Validated PersonaDatosRs personaNucleoFamiliarRs,
            HttpServletResponse response) {
        Contexto ctx = ContextoUtil.obtener();
        personaService.eliminar(ctx, personaNucleoFamiliarRs);
        return ResponseEntity.ok("Ok");
    }
    
        /**
     * Lista todas las personas una persona
     *
     * @param personaNucleoFamiliarRs
     * @param response
     * @return
     */
    /*@ApiOperation(value = "Elimina una persona del sistema.", response = Persona.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Respuesta de la lista de personas donde el atributo codigo representa:"
                + "1. No existen personas registradas.\r\n"
                + "2. Error al eliminar persona.")})
    @GetMapping(value = "/listar-persona",
            produces = {"application/json", "application/xml"},
            consumes = {"application/json", "application/xml"}
    )
    public ArrayList<Persona> listarPersona() {
        return personaRepository.personaRepository.findAll();
    }*/

}
