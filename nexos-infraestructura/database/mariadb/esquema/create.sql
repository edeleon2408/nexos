CREATE DATABASE nexos CHARACTER SET utf8 COLLATE utf8_general_ci;
CREATE USER 'nexos'@'localhost' IDENTIFIED BY 'nexos123' WITH MAX_USER_CONNECTIONS 100;
GRANT ALL PRIVILEGES ON nexos.* TO 'nexos'@'localhost';
CREATE USER 'nexos'@'localhost' IDENTIFIED BY 'nexos123' WITH MAX_USER_CONNECTIONS 20;
GRANT SELECT ON nexos.* TO 'nexos'@'localhost';