# Usuarios
INSERT INTO sys_usuario(usuario_id, version, usuario, clave, lenguaje, timezone, estado, correo_valido, cambiar_clave, fecha_modificacion, intentos_ingreso) VALUES("anonimo", 0, "anonimo", "", "es", "GMT-5", "activo", true, false, CURRENT_TIME, 0);

# Configuraciones
INSERT INTO sys_configuracion(configuracion_id, fecha_modificacion, version, valor, descripcion) VALUES("mail.smtp.host", CURRENT_TIME, 0, "smtp.gmail.com", "Dirección del SMTP para envio de correo.");
INSERT INTO sys_configuracion(configuracion_id, fecha_modificacion, version, valor, descripcion) VALUES("mail.smtp.port", CURRENT_TIME, 0, "587", "Puerto del SMTP para envio de correo.");
INSERT INTO sys_configuracion(configuracion_id, fecha_modificacion, version, valor, descripcion) VALUES("mail.smtp.auth", CURRENT_TIME, 0, "true", "Indica si el SMTP requiere de autenticación.");
INSERT INTO sys_configuracion(configuracion_id, fecha_modificacion, version, valor, descripcion) VALUES("mail.smtp.timeout", CURRENT_TIME, 0, "30000", "Tiempo de espera de conexion al servidor SMTP en segundos.");
INSERT INTO sys_configuracion(configuracion_id, fecha_modificacion, version, valor, descripcion) VALUES("mail.smtp.starttls.enable", CURRENT_TIME, 0, "true", "Indica si el SMTP requiere uso de TSL.");
INSERT INTO sys_configuracion(configuracion_id, fecha_modificacion, version, valor, descripcion) VALUES("sys.identidad.unicoLogueo", CURRENT_TIME, 0, "false", "Indica si el usuario solo se puede loguear una vez de manera concurrente.");
INSERT INTO sys_configuracion(configuracion_id, fecha_modificacion, version, valor, descripcion) VALUES("sys.identidad.tiempoSession", CURRENT_TIME, 0, "0", "Tiempo en segundos que caduca la session si no hay actividad del usuario. Si es 0 no caduca.");
INSERT INTO sys_configuracion(configuracion_id, fecha_modificacion, version, valor, descripcion) VALUES("sys.identidad.cambioClave", CURRENT_TIME, 0, "0", "Numero de dias para solicitar cambio de clave. Si es 0 no se solicita cambio de clave.");
INSERT INTO sys_configuracion(configuracion_id, fecha_modificacion, version, valor, descripcion) VALUES("sys.identidad.intentosBloqueo", CURRENT_TIME, 0, "0", "Numero de reintentos de logueo fallidos con que se bloquea la cuenta del usuario. Si es 0 no se bloquea la cuenta.");
INSERT INTO sys_configuracion(configuracion_id, fecha_modificacion, version, valor, descripcion) VALUES("sys.identidad.esperaBloqueo", CURRENT_TIME, 0, "5", "Tiempo en minutos que dura la cuenta bloqueada por reintentos fallidos.");
