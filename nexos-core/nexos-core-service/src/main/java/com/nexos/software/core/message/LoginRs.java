/*
* Archivo: LoginRs.java
* Fecha: 07/11/2020
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva de NEXOS SOFTWARE
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de NEXOS SOFTWARE
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
 */
package com.nexos.software.core.message;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.time.LocalDateTime;
import javax.validation.constraints.NotBlank;

/**
 *
 * @author Erwin De Leon
 */
public class LoginRs implements Serializable {
    private static final long serialVersionUID = 0L;
    
    @NotBlank
    @ApiModelProperty(notes = "codigo", required = true)
    private int codigo;
    
    @NotBlank
    @ApiModelProperty(notes = "valor", required = true)
    private String valor;
   
    @NotBlank
    @ApiModelProperty(notes = "usuarioToken", required = true)
    private String usuarioToken;
    
    @NotBlank
    @ApiModelProperty(notes = "ultimoIngreso", required = true)
    private LocalDateTime ultimoIngreso;
    
    @NotBlank
    @ApiModelProperty(notes = "tiempoSession", required = true)
    private String tiempoSession;
    
    @NotBlank
    @ApiModelProperty(notes = "tiempoNotificacion", required = true)
    private String tiempoNotificacion;
   /* @NotBlank
    @ApiModelProperty(notes = "primerNombre", required = true)
    private String primerNombre;
    
    @NotBlank
    @ApiModelProperty(notes = "primerApellido", required = true)
    private String primerApellido;
    
     @NotBlank
    @ApiModelProperty(notes = "correo", required = true)
    private String correo;*/
    
     public LoginRs(int codigo, String valor, String usuarioToken, 
             LocalDateTime ultimoIngreso, String tiempoSession,
             String tiempoNotificacion/*, 
             String primerNombre, String primerApellido, String correo*/) {
        super();
        this.codigo = codigo;
        this.valor = valor;
        this.usuarioToken=usuarioToken;
        this.ultimoIngreso=ultimoIngreso;
        this.tiempoSession=tiempoSession;
        this.tiempoNotificacion=tiempoNotificacion;
        /*this.primerNombre=primerNombre;
        this.primerApellido=primerApellido;
        this.correo=correo;*/
    }
     
    public String getUsuarioToken() {
        return usuarioToken;
    }

    public void setUsuarioToken(String usuarioToken) {
        this.usuarioToken = usuarioToken;
    }   

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
    
     public LocalDateTime getUltimoIngreso() {
        return ultimoIngreso;
    }

    public void setUltimoIngreso(LocalDateTime ultimoIngreso) {
        this.ultimoIngreso = ultimoIngreso;
    }    
    
/*
    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }
    */

    public String getTiempoSession() {
        return tiempoSession;
    }

    public void setTiempoSession(String tiempoSession) {
        this.tiempoSession = tiempoSession;
    }

    public String getTiempoNotificacion() {
        return tiempoNotificacion;
    }

    public void setTiempoNotificacion(String tiempoNotificacion) {
        this.tiempoNotificacion = tiempoNotificacion;
    }
    
    
    
}
