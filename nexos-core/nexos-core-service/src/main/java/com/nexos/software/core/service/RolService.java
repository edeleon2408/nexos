/*
* Archivo:RolService.java
* Fecha: 07/11/2020
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva de NEXOS SOFTWARE
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de NEXOS SOFTWARE
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
 */
package com.nexos.software.core.service;

import com.nexos.software.core.entity.PersonaRol;
import com.nexos.software.core.entity.Rol;
import com.nexos.software.system.annotation.Permiso;

public interface RolService {

    /**
     * Busca un rol por Id
     *
     * @param contexto
     * @return 
     */
    @Permiso("nex-rol-buscar")
    Rol buscarPorId( String rol);

    /**
     * 
     * Obtiene todo el rol por su id
     * @author Erwin De León
     * @param idPersona
     * @return
     */
    @Permiso("nex-rol-obtener")
    Rol obtenerRol(String idPersona);
    
    /**
     * Obtiene el rol por el id de persona
     * @param idPersona
     * @return
     */
    PersonaRol obtenerPersonaRol(String idPersona);


}
