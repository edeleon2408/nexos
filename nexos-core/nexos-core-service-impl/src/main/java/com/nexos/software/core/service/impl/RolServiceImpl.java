/*
* Archivo: RolServiceImpl.java
* Fecha: 07/11/2020
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva de NEXOS SOFTWARE
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de NEXOS SOFTWARE
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
 */
package com.nexos.software.core.service.impl;

import com.nexos.software.core.entity.PersonaRol;
import com.nexos.software.core.entity.Rol;
import com.nexos.software.core.service.RolService;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Erwin De León
 */
@Service
public class RolServiceImpl implements RolService {

    private final RolRepository rolRepository;

    public RolServiceImpl(RolRepository rolRepository) {
        this.rolRepository = rolRepository;
    }
    @Autowired
    private PersonaRolRepository personaRolRepository;

    @Override
    public Rol buscarPorId(String rolId) {

        Optional<Rol> optional = this.rolRepository.findById(rolId);
        Rol rol = new Rol();
        if (optional.isPresent()) {
            rol = optional.get();
        }
        return rol;
    }

    @Override
    public Rol obtenerRol(String rol) {

        return rolRepository.findById(rol).orElse(null);
    }

    @Override
    public PersonaRol obtenerPersonaRol(String idPersona) {

        return personaRolRepository.findByPersonaId(idPersona);
    }

}
