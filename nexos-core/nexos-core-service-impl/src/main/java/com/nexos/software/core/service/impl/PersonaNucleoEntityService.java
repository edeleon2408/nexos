/*
* Archivo: PersonaNucleoEntityService.java
* Fecha: 07/11/2020
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva de NEXOS SOFTWARE
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de NEXOS SOFTWARE
* autorizacion por parte de su autor quedan totalmente prohibidos.
*
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
 */
package com.nexos.software.core.service.impl;

import com.nexos.software.system.entity.Persona;
import com.nexos.software.system.entity.Usuario;
import com.nexos.software.system.model.Contexto;
import com.nexos.software.system.service.PersistenciaUtilityService;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Servicio para los metodos TX del negocio al registrar Colaborador.
 *
 * @author Erwin De León 
 */
/*@Service
class PersonaNucleoEntityService {

    private final PersistenciaUtilityService persistenciaUtilityService;

    private final Logger logger = LoggerFactory.getLogger(PersonaNucleoEntityService.class);

    public PersonaNucleoEntityService(PersistenciaUtilityService persistenciaUtilityService) {
        this.persistenciaUtilityService = persistenciaUtilityService;
    }
*/

    /**
     * Metodo para confirmar el registro de la persona en la Tabla Persona
     * @param contexto
     * @param persona
     * @return Persona
     */
    /*@Transactional
    public Persona crearConfirmaRegistroPersona(Contexto contexto, Persona persona) {
        return persistenciaUtilityService.crear(contexto, persona);
    }*/


    /**
     * Metodo para confirmar el registro de un usuario en la Tabla Usuario
     * @param contexto
     * @param usuario
     * @return Usuario
     */
    /*@Transactional
    public Usuario crearConfirmaRegistroUsuario(Contexto contexto, Usuario usuario) {
        return persistenciaUtilityService.crear(contexto, usuario);
    }

}*/
