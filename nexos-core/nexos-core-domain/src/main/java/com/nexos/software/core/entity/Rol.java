/*
* Archivo : Rol.java
* Fecha : 07/11/2020
* Todos los derechos de propiedad intelectual e industrial sobre esta
* aplicacion son de propiedad exclusiva de NEXOS SOFTWARE
* Su uso, alteracion, reproduccion o modificacion sin la debida
* consentimiento por escrito de NEXOS SOFTWARE
* autorizacion por parte de su autor quedan totalmente prohibidos.
* 
* Este programa se encuentra protegido por las disposiciones de la
* Ley 23 de 1982 y demas normas concordantes sobre derechos de autor y
* propiedad intelectual. Su uso no autorizado dara lugar a las sanciones
* previstas en la Ley.
 */
package com.nexos.software.core.entity;
 
import com.nexos.software.system.entity.Catalogo;
import com.nexos.software.system.entity.StandardEntity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author Erwin De León
 */
@Entity
@Table(name = "nex_rol")
public class Rol extends StandardEntity {
    private static final long serialVersionUID = 0L;
    
    @Id
    @Column(name = "rol_id", length = 40)
    private String id;
    @Column(name = "nombre", length = 50, nullable = false)
    private String nombre;
    @Column(name = "descripcion", length = 250, nullable = false)
    private String descripcion;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "catalogo_tipo_rol_id", nullable = false)
    private Catalogo catalogoTipoRol;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Catalogo getCatalogoTipoRol() {
        return catalogoTipoRol;
    }

    public void setCatalogoTipoRol(Catalogo catalogoTipoRol) {
        this.catalogoTipoRol = catalogoTipoRol;
    }
    
    
    
    
}
