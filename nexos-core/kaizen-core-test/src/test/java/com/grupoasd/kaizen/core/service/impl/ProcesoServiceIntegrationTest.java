package com.grupoasd.kaizen.core.service.impl;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import com.grupoasd.kaizen.Application;
import com.grupoasd.kaizen.core.message.ProcesoListarRs;
import com.grupoasd.kaizen.core.service.ProcesoService;
import com.grupoasd.kaizen.system.model.Contexto;
import org.springframework.beans.factory.annotation.Autowired;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = Application.class
)
public class ProcesoServiceIntegrationTest {

    @Autowired
    ProcesoService procesoService;

    private Contexto getContexto() {
        Contexto ctx = new Contexto();
        ctx.put("fhfhfh", "89a4f79dd3a25dce5ee226dc8df3478a");
        return ctx;
    }

    @Test
    public void testListarProcesos() {
        ProcesoListarRs proceso = procesoService.listarProcesosActivos(this.getContexto(), "");
        assertNotNull(proceso.getProcesos());
        assertTrue(!proceso.getProcesos().isEmpty());
    }

    @After
    public void after() {

    }

}
