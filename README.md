# Proyecto Nexos Software #

Proyecto que contiene las librerias de backend y API REST para el proyecto nexos

## Requerimientos ##

* OpenJDK 1.8.0_181 o superior.
* Maven 4 o superior.

## Compilación ##

Ingrese a cada modulo (nexos-system, nexos-core) y en cada carpeta ejecute el siguiente comando maven:

> mvn clean compile install

Luego de esto para compilar el API REST ingrese al módulo nexos-api y ejecute el siguiente comando maven:

> mvn clean compile package

## Generación de paquete de despliegue con docker ##

Ingrese a la carpeta /nexos-api y ejecute el siguiente comando docker:

> docker build -t nexos-api .

## Despliegue utilizando docker ##

Ejecutar el siguiente comando docker reemplazando las xxxx por los valores correspondientes al servidor de base 
de datos y cambiando el valor del parametro --name que contiene la cadena nexos-api por el nombre que se le 
quiera dar al contenedor:

> docker run --name nexos-api -d -p 8080:8080 -e DATABASE_HOST=xxxx \
-e DATABASE_PORT=xxxx -e DATABASE_NAME=xxxx -e DATABASE_USER=xxxx \
-e DATABASE_PASSWORD=xxxx -e DATABASE_POOL=64 nexos-api 

## Documentación del API ##

Se puede visualizar la documentación Swagger del API REST ejecutando el API e ingresando a las siguientes URLs:

Swagger: 

http://localhost:8080/api/v1/v2/api-docs

Documentación amigable de Swagger: 

http://localhost:8080/api/v1/swagger-ui.html


